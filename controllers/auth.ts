var CryptoJS = require("crypto-js");
var express = require('express');
var router = express.Router();
var cors = require('cors');

router.options('*', cors());

router.post('/sendAuthTokenToUser', function (request, response) {
  request.app.locals.db.collection(process.env.MONGODB_PLAYERS_COLLECTION).findOne({ playerName: request.body.playerName }, function(err, doc) {
    if (err) throw err;

    if (doc) {
        let discordData = doc.discordUser.split('#');
        var recipient = request.app.locals.discordClient.users.find(user => user.username === discordData[0] && user.discriminator === discordData[1]);
        if (recipient === null)
            response.status(200).send(JSON.stringify({ message: 'discord recipient not found', action: '/auth' }));

        var hash = CryptoJS.SHA256(doc.playerName + doc.allyCode + doc.discordUser + new Date());
        let token = hash.toString(CryptoJS.enc.Hex);

        let newToken = {
            discordUser: doc.discordUser,
            token: token,
            createdDate: + new Date(),
            ttl: + new Date() + 600000, // now + 10 minutes
            selectedAction: '/' + request.body.selectedAction,
            isUsed: false
        };
        let update = { discordUser: doc.discordUser };
        let options = { upsert: true };

        request.app.locals.db.collection(process.env.MONGODB_LOGINTOKENS_COLLECTION).findOneAndUpdate(update, { $set: newToken }, options, function (err) {
            if (err) throw err;
            recipient.send("login link: " + process.env.FRONTEND_URL + '/auth/' + newToken.token);
            return response.status(200).send(JSON.stringify({ message: 'token sent' }));
        });
    } else {
        response.status(200).send(JSON.stringify({ message: 'not found' }));
    }
  });
});

router.post('/verifyAuthToken', function (request, response) {
  request.app.locals.db.collection(process.env.MONGODB_LOGINTOKENS_COLLECTION).findOne({ token: request.body.token }, function(err, doc) {
    if (err) throw err;
    if (doc) {
        let userToken = doc;
        if (userToken.isUsed) {
            return response.status(401).send(JSON.stringify({ message: 'Your token has already been used, please authenticate again', action: '/auth' }));
        }
        else if (+ new Date() <= userToken.ttl) {
            userToken.isUsed = true;
            let update = { token: userToken.token };
            let options = { upsert: true };
            request.app.locals.db.collection(process.env.MONGODB_LOGINTOKENS_COLLECTION).updateOne(update, { $set: userToken }, options, function (err) {
                if (err) throw err;

                var hash = CryptoJS.SHA256(userToken.discordUser + userToken.token + new Date());
                let sessionId = hash.toString(CryptoJS.enc.Hex);

                let newSession = {
                    discordUser: userToken.discordUser,
                    sessionId: sessionId,
                    createdDate: + new Date(),
                    ttl: + new Date() + 1800000, // now + 30 minutes
                };
                let update = { discordUser: userToken.discordUser };
                let options = { upsert: true };

                request.app.locals.db.collection(process.env.MONGODB_SESSIONS_COLLECTION).findOneAndUpdate(update, { $set: newSession }, options, function (err) {
                    if (err) throw err;

                    return response.status(200).send(JSON.stringify({
                        message: 'Token valid, welcome',
                        action: userToken.selectedAction,
                        authUser: userToken.discordUser,
                        sessionId: sessionId
                    }));
                });
            });
        } else {
            return response.status(401).send(JSON.stringify({ message: 'Your token is expired, please authenticate again', action: '/auth' }));
        }
    } else {
        return response.status(401).send(JSON.stringify({ message: 'Token not found, please authenticate again', action: '/auth' }));
    }
  });
});

router.post('/verifySessionId', function (request, response) {
  request.app.locals.db.collection(process.env.MONGODB_SESSIONS_COLLECTION).findOne({ discordUser: request.body.authUser, sessionId: request.body.sessionId }, function (err, doc) {
      if (err) throw err;

      if (doc) {
          let session = doc;
          if (+ new Date() <= session.ttl) {
              session.ttl = + new Date() + 1800000; // now + 30 minutes
              let update = { discordUser: session.discordUser };
              let options = { upsert: true };
              request.app.locals.db.collection(process.env.MONGODB_SESSIONS_COLLECTION).findOneAndUpdate(update, { $set: session }, options, function (err) {
                  if (err) throw err;
                  return response.status(200).send(true);
              });
          } else {
              return response.status(401).send(JSON.stringify({ message: 'Your session is expired, please authenticate again', action: '/auth' }));
          }
      } else {
          return response.status(401).send(JSON.stringify({ message: 'Session not found, please authenticate again', action: '/auth' }));
      }
  });
});

router.get('/ping',function (request, response) {
    response.send(JSON.stringify({ message: 'pong' }));
});

module.exports = router;