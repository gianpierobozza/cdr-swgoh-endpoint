var express = require('express');
var router = express.Router();
var cors = require('cors');
var ObjectID = require('mongodb').ObjectID;

router.options('*', cors());

router.get('/getPlayerSquads',function (request, response) {
  request.app.locals.db.collection(process.env.MONGODB_PLAYERS_COLLECTION).findOne({ discordUser: request.query.discordUser }, function(err, doc) {
    if (err) throw err;

    if (doc) {
      let playerRoster;
      let squadsInfo = [];
      let squadsDetails = [];
      fetchPlayerRoster(doc.allyCode, request.app.locals.swapi).then(result => {
        playerRoster = result;

        let playerSquads = request.app.locals.db.collection(process.env.MONGODB_SQUADS_COLLECTION).find({ playerName: doc.playerName });
        playerSquads.toArray(function(err, docs) {
          if (err) throw err;

          docs.forEach(doc => {
            let squadTotalGP = 0;
            let units = doc.members.map(member => playerRoster.filter(unit => {
              if (unit.defId === member) {
                squadTotalGP += unit.gp;
                return unit;
              }
            }));
            squadsDetails.push(units);
            doc['totalGP'] = squadTotalGP;
          });
          squadsInfo.push(docs);

          response.status(200).send(JSON.stringify({ 'squadsInfo': squadsInfo, 'squadsDetails': squadsDetails }));
        });
      });
    } else {
        response.status(200).send(JSON.stringify({ message: 'not found' }));
    }
  });
});

router.get('/getPlayerRoster',function (request, response) {
  request.app.locals.db.collection(process.env.MONGODB_PLAYERS_COLLECTION).findOne({ discordUser: request.query.discordUser }, function(err, doc) {
    if (err) throw err;

    if (doc) {
      fetchPlayerRoster(doc.allyCode, request.app.locals.swapi).then(result => {
        response.status(200).send(JSON.stringify(result));
      });
    } else {
      response.status(200).send(JSON.stringify({ message: 'not found' }));
    }
  });
});

router.post('/saveSquad', function (request, response) {
  request.app.locals.db.collection(process.env.MONGODB_PLAYERS_COLLECTION).findOne({ discordUser: request.body.discordUser }, function(err, doc) {
    if (err) throw err;

    if (doc) {
      let query = {};
      if (request.body.id !== "")
        query = { _id: ObjectID(request.body.id) };
      else
        query = { _id: new ObjectID() };

      let update = {};
      update['playerName'] = doc.playerName;
      update['squadName'] = request.body.squadName;
      update['members'] = [
        request.body.squadLeader.defId,
        request.body.squadMemberA.defId,
        request.body.squadMemberB.defId,
        request.body.squadMemberC.defId,
        request.body.squadMemberD.defId
      ];

      let options = { upsert: true };
      request.app.locals.db.collection(process.env.MONGODB_SQUADS_COLLECTION).updateOne(query, { $set: update }, options, function (err) {
        response.status(200).send(JSON.stringify({ message: 'saved' }));
      });
    }
  });
});

router.get('/removeSquad',function (request, response) {
  request.app.locals.db.collection(process.env.MONGODB_SQUADS_COLLECTION).deleteOne({ _id: ObjectID(request.query.id) }, function(err, doc) {
    if (err) throw err;

    if (doc) {
      response.status(200).send(JSON.stringify({ message: 'deleted id: '+ request.query.id }));
    } else {
      response.status(200).send(JSON.stringify({ message: 'not found' }));
    }
  });
});

async function fetchPlayerRoster(allyCode, swapi) {
  let payload = { allycode: allyCode, enums: true, project: { roster: true }};
  let { result, error, warning } = await swapi.fetchPlayer(payload);
  return result[0].roster;
}

module.exports = router;