var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var app = express();

var MongoClient = require('mongodb').MongoClient;

var Discord = require('discord.js');
var discordClient = new Discord.Client();
discordClient.once('ready', () => {
  console.log(`Logged in as ${discordClient.user.tag}!`);
});

var ApiSwgohHelp = require('api-swgoh-help');
var swapi = new ApiSwgohHelp({
    "username": process.env.SWAPI_USERNAME,
    "password": process.env.SWAPI_PASSWORD
});

async function getToken() {
    return await swapi.connect();
}

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(require('./controllers/auth.ts'));
app.use(require('./controllers/squads.ts'));

let mongoDbUri = process.env.MONGODB_CONNECTION_URL;
let mongoDbOptions = { useNewUrlParser: true };
if (process.env.NODE_ENV !== 'production') {
  mongoDbUri += process.env.MONGODB_DB_NAME;
}
let mongoDbClient;

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  MongoClient.connect(mongoDbUri, mongoDbOptions).then(client => {
    const db = client.db(process.env.MONGODB_DB_NAME);
    mongoDbClient = client;
    app.locals.db = db;
  }).catch(error => {
    console.error(error)
  });
  app.locals.discordClient = discordClient;
  app.locals.swapi = swapi;
});

discordClient.login(process.env.DISCORD_CLIENT_LOGIN_SECRET).then().catch(console.error);

process.on('SIGINT', () => {
  mongoDbClient.close();
  process.exit();
});
